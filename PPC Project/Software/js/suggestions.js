
//Adds text in the text area to a div underneath the box. 
function myFunction() {
	if(localStorage.text) {
		localStorage.text = localStorage.text + "<br>" + "<br>" + document.getElementById('textfield').value;
	} else {
		localStorage.text = document.getElementById('textfield').value + "\n";
	}
	document.getElementById("result").innerHTML = localStorage.text;
}

//Clears out the localStorage
function clearAll() {
	localStorage.clear();
}

window.onload = function pageCount() {
	if(document.readystate = "complete") {
		if(localStorage.text) {
			document.getElementById("result").innerHTML = localStorage.text;
		} else {
			document.getElementById("result").innerHTML = "";
		}
	}
}